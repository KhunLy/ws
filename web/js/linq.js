const Generator = Object.getPrototypeOf(function* () {});

/**
 * Param Array array
 * Return Generator
 */
function* _(array){
    yield* array;
}

/**
 * @param predicate
 */
Generator.prototype.each = function(predicate){
    for (let item of this) {
        predicate(item);
    }
};


/**
 * Return Array
 */
Generator.prototype.toList = function(){
    let result = [];
    for (let item of this) {
        result.push(item);
    }
    return result;
};

/**
 * Param Function predicate
 * Return Generator
 */
Generator.prototype.where = function*(predicate){
    for (let item of this) {
        //console.log("where");
        if (predicate(item)) {
            yield item;
        }
    }
};

/**
 * Param Function predicate
 * Return Generator
 */
Generator.prototype.select = function*(predicate){
    for (let item of this) {
        //console.log("select");
        yield predicate(item);
    }
};

/**
 * Param Function predicate
 * Return Object|null
 */
Generator.prototype.first = function(predicate){
    if (predicate === undefined) {
        return this.next().value;
    }
    else{
        for (let item of this) {
            if (predicate(this)) {
                return item;
            }
        }
    }
    return null;
};

/**
 * Param Function predicate
 * Return Generator
 */
Generator.prototype.top = function*(value){
    let cpt = 0;
    for (item of this) {
        yield item;
        if(value === ++cpt){
            this.return();
        }
    }
};

/**
 * Param Array|Generator list
 * Param Function comparedPropFrom
 * Param Function comparedPropTo
 * Param Function predicate
 * Return Generator
 */
Generator.prototype.join = function*(list, comparedPropFrom, comparedPropTo, predicate){
    for (item1 of this) {
        for (item2 of list) {
            if (comparedPropFrom(item1) === comparedPropTo(item2)) {
                yield predicate(item1, item2);
            }
        }
    }
};