<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\RegisterType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends FOSRestController
{

    /**
     * @param Request $request
     * @return User|\Symfony\Component\Form\FormInterface
     * @Rest\Post(path="/user")
     * @Rest\View(serializerGroups={"register"})
     */
    public function postAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $user->setRole("ROLE_USER");
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $em->persist($user);
            $em->flush();
            return $user;
        }
        return $form;
    }
}
