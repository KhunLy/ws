<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\ChangeAvatarType;
use AppBundle\Form\RegisterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends Controller
{
    /**
     * @Route(name="login", path="/login")
     */
    public function loginAction(Request $request, AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();
        return $this->render(':User:login.html.twig', [
            "error" => $error,
            "username" => $username,
            "form" => $this->createForm(RegisterType::class)->createView(),
        ]);
    }

    /**
     * @Route(name="profil", path="/profil")
     * @Security("has_role('ROLE_USER')")
     */
    public function profilAction(Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId());
        $form = $this->createForm(ChangeAvatarType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            /**
             * @var $user User
             */
            $filename = uniqid() . $user->getFile()->getClientOriginalName();
            $user->getFile()->move(
                $this->getParameter("avatar_directory"), $filename
            );
            $user->setFile(null);
            $user->setAvatar("img/avatar/" . $filename);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute("chat_index");
        }
        return $this->render(":User:profil.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
