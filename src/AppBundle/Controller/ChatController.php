<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends Controller
{
    /**
     * @Route(name="chat_index", path="/chat")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction()
    {
        return $this->render(':Chat:index.html.twig',[
        ]);
    }
}
