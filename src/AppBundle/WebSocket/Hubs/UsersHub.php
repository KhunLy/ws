<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 15:22
 */

namespace AppBundle\WebSocket\Hubs;


use AppBundle\Entity\User;
use Kly\WebSocketBundle\Core\BaseHub;

class UsersHub extends BaseHub
{

    /**
     * @param $data
     * @return array<string,object>
     */
    protected function register($data)
    {
        $uRepo = $this->getDoctrine()->getRepository(User::class);
        $result = [];
        foreach ($this->getConnectedUsers() as $username){
            $result[$username] = array_map(function($username)use($uRepo){
                $user = $uRepo->findOneBy(["username" => $username]);
                $this->em->refresh($user);
                return $user;
            }, $this->getConnectedUsers());
        }
        return $result;
    }

    /**
     * @param $data
     * @return array<string,object>
     */
    protected function post($data)
    {
        // TODO: Implement onPost() method.
    }

    /**
     * @param $data
     * @return array<string,object>
     */
    protected function put($data)
    {
        // TODO: Implement onPut() method.
    }

    /**
     * @param $data
     * @return array<string,object>
     */
    protected function delete($data)
    {
        // TODO: Implement onDelete() method.
    }
}