<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 15:50
 */

namespace AppBundle\WebSocket\Hubs;


use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Kly\WebSocketBundle\Core\BaseHub;

class MessagesHub extends BaseHub
{

    /**
     * @param $data
     * @return array<string,object>
     */
    protected function post($data)
    {
        $this->debug($data);
        $uRepo = $this->getDoctrine()->getRepository(User::class);
        $author = $data["author"];
        $receiver = $data["receiver"];
        $content = $data["content"];
        $timestamp = $data["date"];
        $message = (new Message())
            ->setDate((new \DateTime())->setTimestamp($timestamp))
            ->setAuthor($uRepo->findOneBy(["username" => $author]))
            ->setReceiver($uRepo->findOneBy(["username" => $receiver]))
            ->setContent($content);
        ;
        $this->em->persist($message);
        $this->em->flush();
        $mRepo = $this->getDoctrine()->getRepository("AppBundle:Message");
        return [
            $this->currentUsername => $mRepo->getMessages($this->currentUsername),
            $receiver => $mRepo->getMessages($receiver),
        ];
    }

    /**
     * @param $data
     * @return array<string,object>
     */
    protected function put($data)
    {
        // TODO: Implement put() method.
    }

    /**
     * @param $data
     * @return array<string,object>
     */
    protected function delete($data)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param $data
     * @return array<string,object>
     */
    protected function register($data)
    {
        $mRepo = $this->getDoctrine()->getRepository("AppBundle:Message");
        return [
            $this->currentUsername => $mRepo->getMessages($this->currentUsername)
        ];
    }
}