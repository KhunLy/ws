<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 10:52
 */

namespace Kly\WebSocketBundle\Command;


use Kly\WebSocketBundle\Core\HubContext;
use Ratchet\App;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class RunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName("kl:ws:run");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $host = $this->getContainer()->getParameter("kly_web_socket.host");
        $port = $this->getContainer()->getParameter("kly_web_socket.port");
        $routingFiles = $this->getContainer()->getParameter("kly_web_socket.routing_files");
        $server = new App($host, $port);
        foreach ($routingFiles as $routingFile){
            $routes = Yaml::parseFile($routingFile);
            foreach ($routes as $route){
                $path = $route["path"];
                $hubName = $route["hub"];
                $server->route($path, new $hubName(
                    $this->getContainer(),
                    new HubContext()
                ));
            }
        }
        $output->write(
            sprintf(
                "Run WebSocket Server on %s:%d",
                $host,
                $port
            )
        );
        $server->run();

    }
}