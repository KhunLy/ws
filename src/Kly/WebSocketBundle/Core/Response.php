<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 12:37
 */

namespace Kly\WebSocketBundle\Core;


class Response
{
    private $data;
    private $timestamp;
    private $caller;
    private $type;

    public function __construct($type, $data, $caller)
    {
        $this->type = $type;
        $this->data = $data;
        $this->caller = $caller;
        $this->timestamp = (new \DateTime())->getTimestamp();
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Response
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return Response
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     * @return Response
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCaller()
    {
        return $this->caller;
    }

    /**
     * @param mixed $caller
     * @return Response
     */
    public function setCaller($caller)
    {
        $this->caller = $caller;
        return $this;
    }
}