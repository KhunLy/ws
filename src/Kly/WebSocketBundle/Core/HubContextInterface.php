<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 11:09
 */

namespace Kly\WebSocketBundle\Core;


use Ratchet\ConnectionInterface;

interface HubContextInterface
{
    /**
     * @param $key
     * @param ConnectionInterface $conn
     * @return mixed
     */
    function add($key, ConnectionInterface $conn);

    /**
     * @param ConnectionInterface $conn
     * @return mixed
     */
    function remove(ConnectionInterface $conn);

    /**
     * @param $key
     * @return mixed
     */
    function removeAt($key);

    /**
     * @param $key
     * @return ConnectionInterface
     */
    function get($key);

    /**
     * @return ConnectionInterface[]
     */
    function getAll();

    /**
     * @return array
     */
    function getKeys();

    /**
     * @param ConnectionInterface $conn
     * @return bool
     */
    function isConnected(ConnectionInterface $conn);
}