<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 12:37
 */

namespace Kly\WebSocketBundle\Core;


use JMS\Serializer\Annotation as Serializer;

class Request
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $token;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    private $type;

    /**
     * @var array<string, string>
     * @Serializer\Type("array<string, string>")
     *
     */
    private $data;

    /**
     * @var float
     * @Serializer\Type("string")
     */
    private $timestamp;


    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return Request
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Request
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }



    /**
     * @return array<string,string>
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array<string, string> $data
     * @return Request
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return float
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param float $timestamp
     * @return Request
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }
}