<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 12:35
 */

namespace Kly\WebSocketBundle\Core;


use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\MissingTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Psr\Container\ContainerInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Ratchet\Wamp\Exception;

abstract class BaseHub implements MessageComponentInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var HubContext
     */
    private $context;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    protected $em;

    /**
     * @var \JMS\Serializer\Serializer
     */
    private $serializer;

    /**
     * @var string
     */
    protected $currentUsername;

    public function __construct(ContainerInterface $container, HubContextInterface $context)
    {
        $this->container = $container;
        $this->context = $context;
        $this->em = $this->getDoctrine()->getManager();
        $this->serializer = $this->container->get("jms_serializer");
    }

    /**
     * @param $id
     * @return object
     */
    protected function get($id)
    {
        if(!$this->container->has($id)){
            throw new \LogicException();
        }
        return $this->container->get($id);
    }

    /**
     * @return \Doctrine\Bundle\DoctrineBundle\Registry
     */
    protected function getDoctrine()
    {
        if (!$this->container->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application. Try running "composer require symfony/orm-pack".');
        }

        return $this->container->get('doctrine');
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    function onOpen(ConnectionInterface $conn)
    {
        // TODO: Implement onOpen() method.
        echo "new user\n";
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    function onClose(ConnectionInterface $conn)
    {
        $this->context->remove($conn);
        echo "user leave\n";
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo sprintf("ERROR %s:\n%s\n", $e->getCode(), $e->getMessage());
        $conn->send($this->serializer->serialize(new Response("onError", $e->getMessage(), $this->currentUsername), "json"));
    }

    /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $from The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    function onMessage(ConnectionInterface $from, $msg)
    {
        $data = $this->serializer->deserialize($msg, Request::class, "json");
        /**
         * @var $data Request
         */
        $tokenString = $data->getToken();
        if ($tokenString === null){
            throw new MissingTokenException();
        }
        $token = new JWTUserToken();
        $token->setRawToken($tokenString);
        $payload = $this->container
            ->get("lexik_jwt_authentication.jwt_manager")->decode($token);
        if($payload["exp"] < (new \DateTime())->getTimestamp())
        {
            throw new ExpiredTokenException("Your token has expired");
        }
        $this->currentUsername = $payload["username"];
        if(!$this->context->isConnected($from)){
            $this->context->add($this->currentUsername, $from);
        }
        $methodName = $data->getType();
        foreach ($this->$methodName($data->getData()) as $key => $value){
            $this->update("on" . ucwords($methodName), $key, $value);
        }

    }

    /**
     * @param $data
     * @return array<string,object>
     */
    abstract protected function post($data);

    /**
     * @param $data
     * @return array<string,object>
     */
    abstract protected function put($data);

    /**
     * @param $data
     * @return array<string,object>
     */
    abstract protected function delete($data);

    /**
     * @param $data
     * @return array<string,object>
     */
    abstract protected function register($data);

    protected function debug($data)
    {
        echo "DEBUG:\n" . json_encode($data) . "\n";
    }

    /**
     * @param $username string
     * @param $data
     */
    private function update($type, $username, $data){
        $this->context->get($username)->send(
            $this->serializer->serialize(
                new Response($type, $data, $this->currentUsername), "json"
            )
        );
    }

    /**
     * @return array
     */
    protected function getConnectedUsers(){
       return $this->context->getKeys();
    }
}