<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 11:10
 */

namespace Kly\WebSocketBundle\Core;


use Ratchet\ConnectionInterface;

class HubContext implements HubContextInterface
{

    /**
     * @var ConnectionInterface[]
     */
    private $connections;

    public function __construct()
    {
        $this->connections = [];
    }

    /**
     * @param $key
     * @param ConnectionInterface $conn
     * @return mixed
     */
    function add($key, ConnectionInterface $conn)
    {
        $this->connections[$key] = $conn;
    }

    /**
     * @param ConnectionInterface $conn
     * @return mixed
     */
    function remove(ConnectionInterface $conn)
    {
        foreach ($this->connections as $key => $connection){
            if ($connection === $conn){
                unset($this->connections[$key]);
            }
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    function removeAt($key)
    {
        unset($this->connections[$key]);
    }

    /**
     * @param $key
     * @return ConnectionInterface|null
     */
    function get($key)
    {
        return $this->connections[$key];
    }

    /**
     * @return ConnectionInterface[]
     */
    function getAll()
    {
        return $this->connections;
    }

    /**
     * @return array
     */
    function getKeys()
    {
        return array_keys($this->connections);
    }

    /**
     * @param ConnectionInterface $conn
     * @return bool
     */
    function isConnected(ConnectionInterface $conn)
    {
        return in_array($conn, $this->connections);
    }
}