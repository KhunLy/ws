/**
 * @returns {string}
 */
String.prototype.ucwords = function(){
    return this.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
};

/**
 *
 * @param $type {string}
 * @param $token {string}
 * @param $data {Object[]}
 * @constructor
 */
function Request($type, $token, $data){
    /**
     * @type {string}
     */
    this.type = $type;
    /**
     * @type {string}
     */
    this.token = $token;
    /**
     * @type {Object[]}
     */
    this.data = $data;
    /**
     * @type {number}
     */
    this.timestamp = (new Date()).getTime() / 1000;
}

/**
 *
 * @param $type {string}
 * @param $data {Object[]}
 * @param $timestamp {number}
 * @param $caller {string}
 * @constructor
 */
function Event($type, $data, $timestamp, $caller){
    /**
     * @type {string}
     */
    this.type = $type;
    /**
     * @type {Object[]}
     */
    this.data = $data;
    /**
     * @type {number}
     */
    this.timestamp = $timestamp;
    /**
     * @type {string}
     */
    this.caller = $caller;
}

/**
 * @return {string}
 */
Event.prototype.getType = function () {
    return $this.type;
};

/**
 * @return {Object[]}
 */
Event.prototype.getData = function(){
    return this.data;
};

/**
 * @return {string}
 */
Event.prototype.getCaller = function(){
    return this.caller;
};

/**
 * @return {number}
 */
Event.prototype.getTimestamp = function(){
    return this.timestamp;
};

/**
 *
 * @param $host {string}
 * @param $port {number}
 * @param $path {string}
 * @param $token {string}
 * @constructor
 */
function WS($host, $port, $path, $token) {
    /**
     * @type {string}
     */
    this.host = $host;
    /**
     * @type {number}
     */
    this.port = $port;
    /**
     * @type {string}
     */
    this.path = $path;
    /**
     * @type {string}
     */
    this.token = $token;
    /**
     * @type {WS}
     */
    let instance = this;

    /**
     * @type {Array<string,function>}
     */
    this.callbacks = {
        onRegister: ($event) => {
            console.log($event);
        },
        onPost: ($event) => {
            console.log($event);
        },
        onPut: ($event) => {
            console.log($event);
        },
        onDelete: ($event) => {
            console.log($event);
        },
        onError: ($event) => {
            console.log($event);
        }
    };

    /**
     * @type {WebSocket}
     */
    let conn = new WebSocket(`ws://${$host}:${$port}${$path}`);
    this.conn = conn;
    this.conn.onopen = function(){
        conn.send(JSON.stringify(new Request("register", $token)));
    };
    this.conn.onmessage = function(dataString){
        let data = JSON.parse(dataString.data);
        let event = new Event(data["type"], data["data"], data["timestamp"], data["caller"]);
        instance.callbacks[data["type"]](event);
    };
}

/**
 * @param $name {string}
 * @param $callback {function}
 */
WS.prototype.on = function($name, $callback){
    this.callbacks["on" + $name.ucwords()] = $callback;
};

/**
 * @param $callback {function}
 */
WS.prototype.onError = function($callback){
    this.callbacks.onError = $callback;
};

/**
 * @param $callback {function}
 */
WS.prototype.onRegister = function($callback){
    this.callbacks.onRegister = $callback;
};

/**
 * @param $callback {function}
 */
WS.prototype.onPost = function($callback){
    this.callbacks.onPost = $callback;
};

/**
 * @param $callback {function}
 */
WS.prototype.onPut = function($callback){
    this.callbacks.onPut = $callback;
};

/**
 * @param $callback {function}
 */
WS.prototype.onDelete = function($callback){
    this.callbacks.onDelete = $callback;
};

/**
 * @param $data {object}
 */
WS.prototype.post = function($data){
    this.conn.send(JSON.stringify(new Request("post", this.token, $data)));
};

/**
 * @param $data {object}
 */
WS.prototype.put = function($data){
    this.conn.send(JSON.stringify(new Request("put", this.token, $data)));
};

/**
 * @param $data {object}
 */
WS.prototype.delete = function($data){
    this.conn.send(JSON.stringify(new Request("delete", this.token, $data)));
};

/**
 * @param $name {string}
 * @param $data {object}
 */
WS.prototype.call = function($name, $data){
    this.conn.send(JSON.stringify(new Request($name, this.token, $data)));
};