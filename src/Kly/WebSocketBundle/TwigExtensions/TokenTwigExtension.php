<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 07-07-18
 * Time: 11:34
 */

namespace Kly\WebSocketBundle\TwigExtensions;



use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TokenTwigExtension extends AbstractExtension
{
    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;
    private $tokenStorage;

    public function __construct(JWTTokenManagerInterface $jwtManager, TokenStorageInterface $tokenStorage)
    {
        $this->jwtManager = $jwtManager;
        $this->tokenStorage = $tokenStorage;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction("getToken", array($this, "getToken"))
        ];
    }

    public function getToken()
    {
        return $this->jwtManager->create($this->tokenStorage->getToken()->getUser());
    }
}